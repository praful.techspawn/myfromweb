<?php
function calender_new_admin_page()
{
?>
<div id="wpbody" role="main">
<div id="wpbody-content" aria-label="Main content" tabindex="0" style="overflow: hidden;">
	<div class="wrap wpbs-wrap">
    <div id="icon-themes" class="icon32"></div>
    <h2>Edit Calendar</h2>                    
    <div class="postbox-container meta-box-sortables ui-sortable">
	<div class="postbox">
		<div class="handlediv" title="Click to toggle"><br></div>
		<h3 class="hndle ui-sortable-handle">Availability</h3>
		<div class="inside">  
        <p>Assign users to this calendar</p>
     <div>
	<form action="" method="post" class="">    
	 <label>Calender Name</label>
	 <input type='text' name="calname" required />
		 <select name="cal_user" required>
			 <option value="">Select Model</option>
			<?php
    $blogusers = get_users('role=editor');
    // Array of WP_User objects.
    foreach ($blogusers as $user) {
        echo '<option value="' . esc_html($user->ID) . '">' . esc_html($user->display_name) . '</option>';
    }
?>
		 </select>
	 <input type="submit" class="button button-primary button-h2 saveCalendar" value="Assign">
	 </form>
	 </div>          
	</div>  
</div>
<?php
    if (isset($_POST['calname'])) {
        $calender_user = $_POST['cal_user'];
        global $wpdb, $table_prefix;
        $calender_table = $table_prefix . CALENDER_SEARCH_TABLE;
        $calender_data  = $wpdb->get_results("SELECT DISTINCT model_id FROM $calender_table WHERE `model_id`= $calender_user ORDER BY id ASC");
        $temp_id        = sizeof($calender_data);
        if ($temp_id > 0) {
?>
			<div>
			<div class="wrap">
			<div id="message" class="updated notice error is-dismissible below-h2">
			<p>Calender already assigned to model.</p>
			<button type="button" class="notice-dismiss">
				<span class="screen-reader-text">Dismiss this notice.</span>
			</button>
			</div>
			</div>
			<?php
        } else {
?>
			<div class="wrap">
			<div id="message" class="updated notice notice-success is-dismissible below-h2">
			<p>Calender added.</p>
			<button type="button" class="notice-dismiss">
				<span class="screen-reader-text">Dismiss this notice.</span>
			</button>
			</div>
			</div>
		<?php
            $calender_name = $_POST['calname'];
            $current_date  = date('Y-m-d');
            //$cal_status = $_POST[cal_status];
            global $wpdb, $table_prefix;
            $calender_table = $table_prefix . CALENDER_SEARCH_TABLE;
            $sql            = "INSERT INTO $calender_table (model_id,calender_name,date,status) VALUES('$calender_user','$calender_name','$current_date',1);";
            $result         = $wpdb->query($sql);
        }
    }
?>
  </div>
  </div>
  </div>
<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div>
<?php
}
// calender list assign 
function calender_search_list_page()
{
    if ($_POST['del_cal'] == "Delete") {
        $del_request_id = $_POST['del_cal_id'];
        global $wpdb, $table_prefix;
        $calender_table      = $table_prefix . CALENDER_SEARCH_TABLE;
        $calender_table_meta = $table_prefix . CALENDER_SEARCH_META;
        $booking_req         = "DELETE FROM $calender_table WHERE `id` = $del_request_id";
        $result_del          = $wpdb->query($booking_req);
        $booking_req         = "DELETE FROM $calender_table_meta WHERE `cal_id` = $del_request_id";
        $result              = $wpdb->query($booking_req);
        if ($result_del != 0) {
?>
		<div class="wrap">
			<div id="message" class="updated success is-dismissible below-h2">
			<p> <strong> Calender Deleted.</strong> </p>
			<button type="button" class="notice-dismiss">
				<span class="screen-reader-text">Dismiss this notice.</span>
			</button>
			</div>
		</div>
<?php
        }
    }
?>
<div id="wpbody" role="main">
<div id="wpbody-content" aria-label="Main content" tabindex="0" style="overflow: hidden;">
	<div class="wrap wpbs-wrap">
    <div id="icon-themes" class="icon32"></div>
	<div class='wrap'>
	<h2>Add Calender <a href="<?php
    echo admin_url('admin.php?page=new_calender');
?>" class='add-new-h2'>Add New Calender</a></h2>
	</div>
    <h2>Calendar List</h2>
        <div class="postbox-container meta-box-sortables ui-sortable">             
            <div class="metabox-holder">
                <div class="postbox">
                    <div class="handlediv" title="Click to toggle"><br></div>
                    <h3 class="hndle ui-sortable-handle">Bookings</h3>
                    <div class="inside"> <!-- new code here -->
  <table class="wp-list-table widefat fixed striped posts">
	<thead>
	<tr>
		<th scope="col" id="cb" class="manage-column column-cb check-column" style="">
		<label class="screen-reader-text" for="cb-select-all-1">Select All</label>
		<input id="cb-select-all-1" type="checkbox"></th>
		<th scope="col" id="title" class="manage-column column-title sortable desc" style="">
		<a href="http://localhost/wordpress/wp-admin/edit.php?orderby=title&amp;order=asc"><span>Calender Name</span>
		<span class="sorting-indicator"></span></a></th>
		<th scope="col" id="author" class="manage-column column-author" style="">Model</th>
		<th scope="col" id="categories" class="manage-column column-categories" style="">Status</th>

		<th scope="col" id="date" class="manage-column column-date sortable asc" style="">
		<a href="http://localhost/wordpress/wp-admin/edit.php?orderby=date&amp;order=desc"><span>Date</span>
		<span class="sorting-indicator"></span></a></th>	</tr>
	</thead>
	<tbody id="the-list">
	<?php
    global $wpdb, $table_prefix;
    $calender_table   = $table_prefix . CALENDER_SEARCH_TABLE;
    $calender_entries = $wpdb->get_results("SELECT * FROM $calender_table ORDER BY id ASC");
    foreach ($calender_entries as $calender_entry) {
?>
			<tr id="post-1" class="iedit author-self level-0 post-1 type-post status-publish format-standard hentry category-uncategorized">
			<th scope="row" class="check-column">
			<label class="screen-reader-text" for="cb-select-1">Select Hello world!</label>
			<input id="cb-select-1" type="checkbox" name="post[]" value="1">
			<div class="locked-indicator"></div>
			</th>
	<td class="post-title page-title column-title"><strong><?php
        echo $calender_entry->calender_name;
?></strong>
			<div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
			<form method="post" action="">
					<div class="row">
						<input type="hidden" name="del_cal_id" value="<?php
        echo $calender_entry->id;
?>">
						<div class="row-actions">
							<span class="trash">
							<input type="submit" name="del_cal" onclick="return delete_cal()" class="button saveCalendar" value="Delete">
							<!--a class="submitdelete" title="Move this item to the Trash" href="#">Delete</a-->
							</span>
						</div>
					</div>
				</form>
			
			<div class="tags_input" id="post_tag_1"></div><div class="sticky"></div><div class="post_format"></div></div></td>
			<?php
        $user_info   = get_userdata($calender_entry->model_id);
        $mfirst_name = $user_info->first_name;
        $mlast_name  = $user_info->last_name;
?>
			<td class="author column-author"><a href="edit.php?post_type=post&amp;author=1"><?php
        echo $mfirst_name . " " . $mlast_name;
?></a></td>
			<td class="categories column-categories"><span>Assigned</span></td>
			<td class="date column-date"><abbr title="<?php
        echo $calender_entry->date;
?>"><?php
        echo $calender_entry->date;
?></abbr></td>		
		</tr>
		<?php
    }
?>					
	</tbody>
</table>			
					   </div> <!-- code end here -->
                </div>
            </div>
	 </div>          
                    </div>
                </div>
            </div>  
            
            </form>
        </div>
         
    </div>
<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div>
<script>
function delete_cal() {
	var r = confirm("This will also Delete Bookings related to that Calender. Do you want to delete Calender?");
    if (r == true) {
       return true;
    } else {
       return false;
    }
}
</script> 
<?php
}
function calender_search_booking_page()
{
    if ($_POST['request_accept'] == "Accept") {
        $model_request_id   = $_POST['req_id'];
        $model_booking_date = $_POST['booking_date'];
        $model_id           = $_POST['model_uid'];
        global $wpdb, $table_prefix;
        $calender_table_meta = $table_prefix . CALENDER_SEARCH_META;
        $booking_req         = "UPDATE $calender_table_meta SET `status` = 'accepted' WHERE `id` = $model_request_id";
        $result              = $wpdb->query($booking_req);
        $booking_req         = "DELETE FROM $calender_table_meta WHERE `model_id` = $model_id AND `bookingdate` = '$model_booking_date' AND `status` = 'pending'";
        $result              = $wpdb->query($booking_req);
?>
		<div class="wrap">
			<div id="message" class="updated success is-dismissible below-h2">
			<p>Thank you. You are booked on <strong> <?php
        echo $model_booking_date;
?> </strong>  </p>
			<button type="button" class="notice-dismiss">
				<span class="screen-reader-text">Dismiss this notice.</span>
			</button>
			</div>
		</div>
	<?php
    }
    if ($_POST['request_accept'] == "Reject" || $_POST['request_accept'] == "Delete") {
        $model_request_id = $_POST['req_id'];
        global $wpdb, $table_prefix;
        $calender_table_meta = $table_prefix . CALENDER_SEARCH_META;
        $booking_req         = "DELETE FROM $calender_table_meta WHERE `id` = $model_request_id";
        $result              = $wpdb->query($booking_req);
        echo "done rejected";
    }
?>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!--link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script-->
  <!-- booking code -->
	<div class="wrap wpbs-wrap">
    <div id="icon-themes" class="icon32"></div>
    <h2>Booking List</h2>             
        <div class="postbox-container meta-box-sortables ui-sortable">             
            <div class="metabox-holder">
                <div class="postbox">
                    <div class="handlediv" title="Click to toggle"><br></div>
                    <h3 class="hndle ui-sortable-handle">Bookings</h3>
 <div class="inside table-responsive"> <!-- new code here -->
  <table class="display table wp-list-table widefat fixed striped posts">
        <tbody id="the-list">
	<?php
    global $wpdb, $table_prefix;
    $calender_table_meta = $table_prefix . CALENDER_SEARCH_META;
    $calender_metadata   = $wpdb->get_results("SELECT * FROM $calender_table_meta ORDER BY id DESC");
    $i == 0;
    foreach ($calender_metadata as $booking_entry) {
        $i++;
        $c_data = $booking_entry->customer_data;
        $c_data = rtrim($c_data, '***');
        $c_data = explode('***', $c_data);
?>
		<tr id="post-1" class="iedit author-self level-0 post-1 type-post status-publish format-standard hentry category-uncategorized">
			<th scope="row" class="check-column">
			<label id="cb-select-1" type="" value=""><?php
        echo $i;
?></label>
			<div class="locked-indicator"></div>
			</th>
				<td data-toggle="collapse" data-target="#demo<?php
        echo $i;
?>" style="width: 30%;" class="post-title page-title column-title"><strong style="display: inline;">Check In: </strong><?php
        echo $booking_entry->bookingdate;
?>
				<div class="locked-info"><span class="locked-avatar"></span> <span class="locked-text"></span></div>
				<div class="tags_input" id="post_tag_1"></div><div class="sticky"></div><div class="post_format"></div></div></td>
				<td data-toggle="collapse" data-target="#demo<?php
        echo $i;
?>"  class="author column-author"><strong style="display: inline;">Jobart: </strong><?php
        echo $c_data[0];
?></td>
			<td class="categories column-categories">
			<strong style="display: inline;">Status: </strong> 
			<?php
        if ($booking_entry->status == 'accepted') {
            $class = "green";
        } else {
            $class = "red";
        }
        echo '<span class=' . $class . '>' . $booking_entry->status . '</span>';
?>
			</td>
			<td class="date column-date">
			<form method="post" action="">
			<img src=''/>
					<input type="submit" name="request_accept" onclick="return confirm_delete()" class="button" value="Delete" >
			<form>
			</td>
		
		</tr>
		<tr>
			<td style='border-color:white'> </td>
			<td style='border-color:white'>
				 <div id="demo<?php
        echo $i;
?>" class="collapse">
				  <p><strong>Jobart: </strong> <span class="wpbs-booking-field-text-wrap"><?php
        echo $c_data[0];
?> </span></p>
				  <p><strong>Firma: </strong> <span class="wpbs-booking-field-text-wrap"><?php
        echo $c_data[1];
?></span></p>
				  <p><strong>Ansprechpartner: </strong> <span class="wpbs-booking-field-text-wrap"><?php
        echo $c_data[2];
?></span></p>
				  <p><strong>Job: </strong> <span class="wpbs-booking-field-text-wrap"><?php
        $job_data = str_replace("**", ",", $c_data[3]);
        echo $job_data;
?></span></p>
				  <p><strong>Fotograf: </strong> <span class="wpbs-booking-field-text-wrap"><?php
        echo $c_data[4];
?></span></p>
				  <p><strong>Haare & Make up: </strong> <span class="wpbs-booking-field-text-wrap"><?php
        echo $c_data[5];
?></span></p>
				  <p><strong>Ort des Jobs: </strong> <span class="wpbs-booking-field-text-wrap"><?php
        echo $c_data[6];
?></span></p>
				  <p><strong>Arbeitsbeginn: </strong> <span class="wpbs-booking-field-text-wrap"><?php
        echo $c_data[7];
?></span></p>
				  <p><strong>Tagesgage: </strong> <span class="wpbs-booking-field-text-wrap"><?php
        echo $c_data[8];
?></span></p>
				  <p><strong>Email: </strong> <span class="wpbs-booking-field-text-wrap"><?php
        echo $c_data[9];
?></span></p>
				
				<form method="post" action="">
					<div class="row">
						<input type="hidden" name="req_id" value="<?php
        echo $booking_entry->id;
?>">
						<input type="hidden" name="booking_date" value="<?php
        echo $booking_entry->bookingdate;
?>">
						<input type="hidden" name="model_uid" value="<?php
        echo $booking_entry->model_id;
?>">
				
						<?php
        if ($booking_entry->status == 'accepted') {
?>
                       <input type="button" class="button button-danger button-h2 saveCalendar" value="Accept">
						<input type="submit" name="request_accept" onclick="return confirm_delete()" class="button button-danger button-h2 saveCalendar" value="Reject">
						<?php
        } else {
?>
						<input type="submit" name="request_accept" onclick="return confirm_accept()"  class="button button-primary button-h2 saveCalendar" value="Accept">
						<input type="submit" name="request_accept" onclick="return confirm_delete()" class="button button-primary button-h2 saveCalendar" value="Reject">
						<?php
        }
?>
					</div>
				</form>
				</div>
			</td>
			<td style='border-color:white'> </td>
			<td style='border-color:white'> </td>
			<td style='border-color:white'> </td>
		</tr>
		<?php
    }
?>
	</tbody>
</table>
					   </div> <!-- code end here -->
                </div>
            </div>  
	 </div>
 <script>
 function confirm_accept() {
    var r = confirm("Do you want to accept request?");
    if (r == true) {
       return true;
    } else {
       return false;
    }
}
 function confirm_delete() {
    var r = confirm("Are you sure? You want to Reject and Delete request?");
    if (r == true) {
       return true;
    } else {
       return false;
    }
}
 </script>
<?php
}
?>