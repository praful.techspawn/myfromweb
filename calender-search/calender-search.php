<?php
/*--------------------------------------------------------------------------------------
 #
 # Plugin Name: Calender Search
 # Plugin URI : http://techspawn.in
 # Description: Internal plugin for search functionality which assigns calender to model, accepts and reject customer requests and lists Created Calenders
 # Version: 1.0.0
 # Author: rahul
 # Author URI: rahulw.techspawn@gmail.com
 #
 *-------------------------------------------------------------------------------------*/
if ( !function_exists( 'add_action' ) ) {
	echo 'Uh huh! ,Plugin can not do much when called directly.';
	exit;
}
#declare constants
define( 'CALENDER_SEARCH_PLUGIN_URL', plugin_dir_url(__FILE__) );
define( 'CALENDER_SEARCH_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'CALENDER_SEARCH_TABLE','calender');
define( 'CALENDER_SEARCH_META','calender_booking');
require_once (CALENDER_SEARCH_PLUGIN_DIR . '/modules/functions.php');
#register plugin function
register_activation_hook( __FILE__, 'setup_calender_search_ts' );
function setup_calender_search_ts(){
	global $wpdb, $table_prefix;
		$table_name = $table_prefix.CALENDER_SEARCH_TABLE;
		if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
			$sql = "CREATE TABLE $table_name (
						id int(5) NOT NULL AUTO_INCREMENT,
    					UNIQUE KEY id(id),
						model_id int(5) NOT NULL,
						calender_name varchar(100) NOT NULL,
						date date ,
						status  int(5) NOT NULL
					);";
			$rs = $wpdb->query($sql);
		}
	   	$meta_table = $table_prefix.CALENDER_SEARCH_META;
		if($wpdb->get_var("show tables like '$meta_table'") != $meta_table) {
			$sql = "CREATE TABLE $meta_table (
						id int(5) NOT NULL AUTO_INCREMENT,
						UNIQUE KEY id(id),
						model_id int(5) NOT NULL,
						customer_id int(5) NOT NULL,
						status varchar(100) NOT NULL default 'pending',
						customer_data varchar(500) NOT NULL,
						cal_id int(5) NOT NULL,
						createdon date ,
						updatedon date ,
						bookingdate date
					);";
			$rs2 = $wpdb->query($sql);
		}
}
if ( is_admin() ) {
    add_action('admin_menu', 'calender_search_settings');
	wp_enqueue_style( 'calender-search-style', CALENDER_SEARCH_PLUGIN_URL.'css/calender.css',false, 1.0, 'all');
}
function calender_search_settings() {
	add_menu_page('calender_search','Wp Calender List', 'administrator','calender_search-admin', 'calender_search_list_page');
	add_submenu_page( 'calender_search-admin', 'Calender search admin', 'Calender Booking', 'manage_options', 'calender-search-booking', 'calender_search_booking_page');
	add_submenu_page( 'calender_search-admin', 'Calender search admin', 'New Calender', 'manage_options', 'new_calender', 'calender_new_admin_page');
}
?>